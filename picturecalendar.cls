\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{picturecalendar}[2024/02/14 v1.0 Picture calendar]

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{article}}
\ProcessOptions\relax
\LoadClass{article}
\RequirePackage{kvoptions}
\DeclareStringOption[125mm]{paperwidth}
\DeclareStringOption[150mm]{paperheight}
\DeclareStringOption[10mm]{cardmargin}
\DeclareStringOption[3mm]{picturepadding}
\DeclareStringOption[44]{fontsizetitle}
\DeclareStringOption[32]{fontsizemonth}
\DeclareStringOption[5.5]{fontsizedays}
\ProcessKeyvalOptions*

\RequirePackage{fontspec}
\RequirePackage{tabularx}
\RequirePackage{xcolor}
\RequirePackage[paperwidth=\picturecalendar@paperwidth,paperheight=\picturecalendar@paperheight]{geometry}
\RequirePackage[ngerman]{babel}
\RequirePackage{tikz}
\usetikzlibrary{fill.image}

\newlength{\cardmargin}
\setlength{\cardmargin}{\picturecalendar@cardmargin}
\newlength{\picturepadding}
\setlength{\picturepadding}{\picturecalendar@picturepadding}
\newlength{\largepicturesize}
\setlength{\largepicturesize}{\dimexpr\paperwidth-2\cardmargin\relax}
\newlength{\smallpicturesize}
\setlength{\smallpicturesize}{\dimexpr.5\largepicturesize-.5\picturepadding\relax}

\colorlet{title}{black!50}
\colorlet{month}{black!50}
\colorlet{weekday}{black!75}
\colorlet{weekend}{black}
\colorlet{holiday}{red!50!black}
\colorlet{daynumber}{black!80}

\newcommand{\titlefont}{\sffamily}
\newcommand{\monthfont}{\sffamily}
\newcommand{\weekdayfont}{\bfseries}
\newcommand{\weekendfont}{\bfseries}
\newcommand{\holidayfont}{\bfseries}
\newcommand{\daynumberfont}{}
\renewcommand{\arraystretch}{1.5}
\pagestyle{empty}

\usepackage{luacode}
\begin{luacode*}
pc = require("picturecalendar")
\end{luacode*}

\tikzset{
  inner sep=0,
  titlecard/.style={font=\titlefont\fontsize{\picturecalendar@fontsizetitle}{\picturecalendar@fontsizetitle}\selectfont,color=title},
  months/.style={font=\monthfont\fontsize{\picturecalendar@fontsizemonth}{\picturecalendar@fontsizemonth}\selectfont,color=month,text height=0,text depth=0},
  days/.style={font=\daynumberfont\fontsize{\picturecalendar@fontsizedays}{\picturecalendar@fontsizedays}\selectfont}
}

% Card with one square picture and title text below
\newcommand{\titlecard}[3][]{
\begin{tikzpicture}[overlay,remember picture]
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize,fill overzoom image=#3] (image) at ([yshift=-\cardmargin]current page.north) {};
\node[titlecard,anchor=north] (title) at ([yshift=-.5\cardmargin]image.south) {#2};
#1
\end{tikzpicture}
\clearpage
}

% Card with one square picture
\newcommand{\monthsingle}[4][]{
\luadirect{pc_month = #2
pc_year = #3
picturecalendar = {}}
\begin{tikzpicture}[overlay,remember picture]
\node[anchor=south,days] (days) at ([yshift=1.5\cardmargin]current page.south) {\expandafter{\directlua{tex.sprint(pc.generate_table(pc_month, pc_year, picturecalendar))}}};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize,fill overzoom image=#4] (images) at ([yshift=-\cardmargin]current page.north) {};
\node[anchor=south west,months] (monthname) at ([yshift=3mm]days.north west) {\luadirect{tex.print(pc.get_month_name(pc_month))}};
\node[anchor=south east,months] (monthnumber) at ([yshift=3mm]days.north east) {\luadirect{tex.print(pc.get_month_number(pc_month))}};
#1
\end{tikzpicture}
\clearpage
}

% Card with two pictures horizontally
\newcommand{\monthdoubleh}[5][]{
\luadirect{pc_month = #2
pc_year = #3
picturecalendar = {}}
\begin{tikzpicture}[overlay,remember picture]
\node[anchor=south,days] (days) at ([yshift=1.5\cardmargin]current page.south) {\expandafter{\directlua{tex.sprint(pc.generate_table(pc_month, pc_year, picturecalendar))}}};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize] (images) at ([yshift=-\cardmargin]current page.north) {};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\smallpicturesize,fill overzoom image=#4] at (images.north) {};
\node[anchor=south,minimum width=\largepicturesize,minimum height=\smallpicturesize,fill overzoom image=#5] at (images.south) {};
\node[anchor=south west,months] (monthname) at ([yshift=3mm]days.north west) {\luadirect{tex.print(pc.get_month_name(pc_month))}};
\node[anchor=south east,months] (monthnumber) at ([yshift=3mm]days.north east) {\luadirect{tex.print(pc.get_month_number(pc_month))}};
#1
\end{tikzpicture}
\clearpage
}

% Card with two pictures vertically
\newcommand{\monthdoublev}[5][]{
\luadirect{pc_month = #2
pc_year = #3
picturecalendar = {}}
\begin{tikzpicture}[overlay,remember picture]
\node[anchor=south,days] (days) at ([yshift=1.5\cardmargin]current page.south) {\expandafter{\directlua{tex.sprint(pc.generate_table(pc_month, pc_year, picturecalendar))}}};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize] (images) at ([yshift=-\cardmargin]current page.north) {};
\node[anchor=east,minimum width=\smallpicturesize,minimum height=\largepicturesize,fill overzoom image=#4] at (images.east) {};
\node[anchor=west,minimum width=\smallpicturesize,minimum height=\largepicturesize,fill overzoom image=#5] at (images.west) {};
\node[anchor=south west,months] (monthname) at ([yshift=3mm]days.north west) {\luadirect{tex.print(pc.get_month_name(pc_month))}};
\node[anchor=south east,months] (monthnumber) at ([yshift=3mm]days.north east) {\luadirect{tex.print(pc.get_month_number(pc_month))}};
#1
\end{tikzpicture}
\clearpage
}

% Card with three pictures (big one is at the top)
\newcommand{\monthtripleupper}[6][]{
\luadirect{pc_month = #2
pc_year = #3
picturecalendar = {}}
\begin{tikzpicture}[overlay,remember picture]
\node[anchor=south,days] (days) at ([yshift=1.5\cardmargin]current page.south) {\expandafter{\directlua{tex.sprint(pc.generate_table(pc_month, pc_year, picturecalendar))}}};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize] (images) at ([yshift=-\cardmargin]current page.north) {};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\smallpicturesize,fill overzoom image=#4] at (images.north) {};
\node[anchor=south west,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#5] at (images.south west) {};
\node[anchor=south east,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#6] at (images.south east) {};
\node[anchor=south west,months] (monthname) at ([yshift=3mm]days.north west) {\luadirect{tex.print(pc.get_month_name(pc_month))}};
\node[anchor=south east,months] (monthnumber) at ([yshift=3mm]days.north east) {\luadirect{tex.print(pc.get_month_number(pc_month))}};
#1
\end{tikzpicture}
\clearpage
}

% Card with three pictures (big one is at the bottom)
\newcommand{\monthtriplelower}[6][]{
\luadirect{pc_month = #2
pc_year = #3
picturecalendar = {}}

\begin{tikzpicture}[overlay,remember picture]
\node[anchor=south,days] (days) at ([yshift=1.5\cardmargin]current page.south) {\expandafter{\directlua{tex.sprint(pc.generate_table(pc_month, pc_year, picturecalendar))}}};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize] (images) at ([yshift=-\cardmargin]current page.north) {};
\node[anchor=south,minimum width=\largepicturesize,minimum height=\smallpicturesize,fill overzoom image=#4] at (images.south) {};
\node[anchor=north west,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#5] at (images.north west) {};
\node[anchor=north east,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#6] at (images.north east) {};
\node[anchor=south west,months] (monthname) at ([yshift=3mm]days.north west) {\luadirect{tex.print(pc.get_month_name(pc_month))}};
\node[anchor=south east,months] (monthnumber) at ([yshift=3mm]days.north east) {\luadirect{tex.print(pc.get_month_number(pc_month))}};
#1
\end{tikzpicture}
\clearpage
}

% Card with three pictures (big one is on the left)
\newcommand{\monthtripleleft}[6][]{
\luadirect{pc_month = #2
pc_year = #3
picturecalendar = {}}
\begin{tikzpicture}[overlay,remember picture]
\node[anchor=south,days] (days) at ([yshift=1.5\cardmargin]current page.south) {\expandafter{\directlua{tex.sprint(pc.generate_table(pc_month, pc_year, picturecalendar))}}};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize] (images) at ([yshift=-\cardmargin]current page.north) {};
\node[anchor=west,minimum width=\smallpicturesize,minimum height=\largepicturesize,fill overzoom image=#4] at (images.west) {};
\node[anchor=north east,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#5] at (images.north east) {};
\node[anchor=south east,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#6] at (images.south east) {};
\node[anchor=south west,months] (monthname) at ([yshift=3mm]days.north west) {\luadirect{tex.print(pc.get_month_name(pc_month))}};
\node[anchor=south east,months] (monthnumber) at ([yshift=3mm]days.north east) {\luadirect{tex.print(pc.get_month_number(pc_month))}};
#1
\end{tikzpicture}
\clearpage
}

% Card with three pictures (big one is on the right)
\newcommand{\monthtripleright}[6][]{
\luadirect{pc_month = #2
pc_year = #3
picturecalendar = {}}
\begin{tikzpicture}[overlay,remember picture]
\node[anchor=south,days] (days) at ([yshift=1.5\cardmargin]current page.south) {\expandafter{\directlua{tex.sprint(pc.generate_table(pc_month, pc_year, picturecalendar))}}};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize] (images) at ([yshift=-\cardmargin]current page.north) {};
\node[anchor=east,minimum width=\smallpicturesize,minimum height=\largepicturesize,fill overzoom image=#4] at (images.east) {};
\node[anchor=north west,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#5] at (images.north west) {};
\node[anchor=south west,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#6] at (images.south west) {};
\node[anchor=south west,months] (monthname) at ([yshift=3mm]days.north west) {\luadirect{tex.print(pc.get_month_name(pc_month))}};
\node[anchor=south east,months] (monthnumber) at ([yshift=3mm]days.north east) {\luadirect{tex.print(pc.get_month_number(pc_month))}};
#1
\end{tikzpicture}
\clearpage
}

% Card with four square pictures
\newcommand{\monthsquadrupel}[7][]{
\luadirect{pc_month = #2
pc_year = #3
picturecalendar = {}}
\begin{tikzpicture}[overlay,remember picture]
\node[anchor=south,days] (days) at ([yshift=1.5\cardmargin]current page.south) {\expandafter{\directlua{tex.sprint(pc.generate_table(pc_month, pc_year, picturecalendar))}}};
\node[anchor=north,minimum width=\largepicturesize,minimum height=\largepicturesize] (images) at ([yshift=-\cardmargin]current page.north) {};
\node[anchor=north west,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#4] at (images.north west) {};
\node[anchor=north east,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#5] at (images.north east) {};
\node[anchor=south west,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#6] at (images.south west) {};
\node[anchor=south east,minimum width=\smallpicturesize,minimum height=\smallpicturesize,fill overzoom image=#7] at (images.south east) {};
\node[anchor=south west,months] (monthname) at ([yshift=3mm]days.north west) {\luadirect{tex.print(pc.get_month_name(pc_month))}};
\node[anchor=south east,months] (monthnumber) at ([yshift=3mm]days.north east) {\luadirect{tex.print(pc.get_month_number(pc_month))}};
#1
\end{tikzpicture}
\clearpage
}
