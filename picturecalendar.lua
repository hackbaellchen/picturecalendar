local picturecalendar = {}

function get_days_in_month(mnth, yr)
  return os.date('*t',os.time{year=yr,month=mnth+1,day=0})['day']
end

function get_day_name(yr, mnth, d)
  local day_name = os.date("%a", os.time{year=yr,month=mnth,day=d})
  if day_name == "Mon" then
    return "Mo"
  elseif day_name == "Tue" then
    return "Di"
  elseif day_name == "Wed" then
    return "Mi"
  elseif day_name == "Thu" then
    return "Do"
  elseif day_name == "Fri" then
    return "Fr"
  elseif day_name == "Sat" then
    return "Sa"
  elseif day_name == "Sun" then
    return "So"
  else
    return "??"
  end
end

function get_day_number(yr, mnth, d)
  return os.date("%d", os.time{year=yr,month=mnth,day=d})
end

function get_day_key(yr, mnth, d)
  return string.format("%d%02d%02d", yr, mnth, d)
end

function get_day_key_from_seconds(seconds)
  return os.date("%Y%m%d", seconds)
end

function is_weekend(yr, mnth, d)
  weekday = os.date('*t',os.time{year=yr,month=mnth,day=d})['wday']
  if weekday == 1 or weekday == 7 then
    return true
  else
    return false
  end
end

function read_ics_file()
  -- retrieve local file
  local file = io.open("holidays.ics", "r")
  if file then
    local content = file:read("*a")
    file:close()
    return content
  end

  -- if we did not find anything locally, fetch it
  -- from remote
  local http = require("socket.http")
  local result, _, _ = http.request("https://www.officeholidays.com/ics/germany/thuringia")
  return result
end

function picturecalendar.get_month_name(mnth)
  local month_name = os.date("%B", os.time{year=2022,month=mnth,day=1})
  if month_name == "January" then
    return "Januar"
  elseif month_name == "February" then
    return "Februar"
  elseif month_name == "March" then
    return "März"
  elseif month_name == "April" then
    return "April"
  elseif month_name == "May" then
    return "Mai"
  elseif month_name == "June" then
    return "Juni"
  elseif month_name == "July" then
    return "Juli"
  elseif month_name == "August" then
    return "August"
  elseif month_name == "September" then
    return "September"
  elseif month_name == "October" then
    return "Oktober"
  elseif month_name == "November" then
    return "November"
  elseif month_name == "December" then
    return "Dezember"
  else
    return "??"
  end
end

function picturecalendar.get_month_number(mnth)
  return os.date("%m", os.time{year=2022,month=mnth,day=1})
end

function picturecalendar.generate_table(pc_month, pc_year, pc)
  local result = "\\begin{tabularx}{\\dimexpr\\pagewidth-2\\cardmargin\\relax}{"
  for i=1, get_days_in_month(pc_month, pc_year) do
    result = result .. "@{}>{\\centering\\arraybackslash}X"
  end
  result = result .. "@{}} "
  for i=1, get_days_in_month(pc_month, pc_year) do
    local day_name = get_day_name(pc_year, pc_month, i)
    local day_key = get_day_key(pc_year, pc_month, i)
    if picturecalendar.pc_holidays[day_key] then
      result = result .. "\\holidayfont \\textcolor{holiday}{" .. tostring(day_name) .. "} & " -- mark holidays with color
    elseif is_weekend(pc_year, pc_month, i) then
      result = result .. "\\weekendfont \\textcolor{weekend}{" .. tostring(day_name) .. "} & " -- mark weekends with color
    else
      result = result .. "\\weekdayfont \\textcolor{weekday}{" .. tostring(day_name) .. "} & "
    end
  end
  result = string.sub(result, 1, -3) .. "\\\\ " -- remove last ' & ' and append new line
  for i=1, get_days_in_month(pc_month, pc_year) do
    result = result .. "\\daynumberfont \\textcolor{daynumber}{" .. string.format("%02d", i) .. "} & "
  end
  result = string.sub(result, 1, -3) .. "\\\\ " -- remove last ' & ' and append new line
  result = result .. "\\end{tabularx}"
  return result
end

local ics = read_ics_file()
local ical_bib = require("lua-ical/ical")
local ical = ical_bib.new(ics)
local events = ical_bib.events(ical)

picturecalendar.pc_holidays = {}
for _, event in ipairs(events) do
  picturecalendar.pc_holidays[get_day_key_from_seconds(event.DTSTART)] = true
end

return picturecalendar
