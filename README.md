# LaTeX picture calendar

This is a repository that provides a LaTeX document class to create small standing picture calendars. These feature different layouts of pictures (its primary purpose), weekday names with numbers (it's still a calendar) and marks holidays with color (loaded with lua from an ics file).

An example (in German) can be found in [example.pdf](example.pdf). You can also compile it yourself with `arara example.tex` or using LuaLaTeX directly. Note that you may need to install the required fonts and provide a holiday ics file.

## Changing options and styles

The document class has many predefined values, most of which can be overwritten with options. These are the default values:

- `paperwidth=125mm` defines the width of the cards
- `paperheight=150mm` defines the height of the cards
- `cardmargin=10mm` defines the top, right and left card margins. The bottom margin has a factor of 1.5
- `picturepadding=3mm` defines the padding in grids on cards with more than one picture
- `fontsizetitle=44` defines the font size on the title page
- `fontsizemonth=32` defines the font size of month names and numbers
- `fontsizedays=5.5` defines the font size of the day names and numbers

One can also override the default colors and font styles:

```tex
\colorlet{title}{black!50}              % title card
\colorlet{month}{black!50}              % month names and numbers
\colorlet{weekday}{black!80}            % weekday names
\colorlet{weekend}{black!99}            % weekend names
\colorlet{holiday}{blue!60!black}       % holiday names
\colorlet{daynumber}{black!80}          % day numbers
\renewcommand{\titlefont}{\sffamily}    % title card
\renewcommand{\monthfont}{\sffamily}    % month names and numbers
\renewcommand{\weekdayfont}{\bfseries}  % weekday names
\renewcommand{\weekendfont}{\bfseries}  % weekend names
\renewcommand{\holidayfont}{\bfseries}  % holiday names
\renewcommand{\daynumberfont}{}         % day numbers
\renewcommand{\arraystretch}{1.5}       % spacing between day names and numbers
```

The default locale is German. If needed, it can only be changed in the class itself (once as an option when loading `babel` and also as hardcoded translations in `picturecalendar.lua`)

Currently, eight card styles and one title page are available as seen in the example. All macros feature the same two first arguments (month and year of the card), followed by the image filenames (between one and four, depending on the used layout). Every macro also features an optional argument which is appended to all tikzpicture code. In the example you can see a heart emoji placed at the top left corner of the title card image.

Holidays are fetched from a provided ics file (`holidays.ics`), which has to be downloaded beforehand (the lua http request used in [picturecalendar.lua](picturecalendar.lua) does not support https requests). The url is also hardcoded there, feel free to improve the code to something dynamic.
